`timescale 1ns / 1ns

module client_tx_s_tb;

reg clk;
integer cc;
initial begin
    if ($test$plusargs("vcd")) begin
        $dumpfile("client_tx_s.vcd");
        $dumpvars(5,client_tx_s_tb);
    end
    for (cc=0; cc<450; cc=cc+1) begin
        clk=0; #4;  // 125 MHz * 8bits/cycle -> 1 Gbit/sec
        clk=1; #4;
    end
end

reg rx_ready = 0;
reg tx_ack = 0;
reg tx_warn = 0;
wire tx_req;
wire [13:0] tx_len;
wire [7:0] packet_out;

reg [7:0] s_tdata = 0;
reg s_tvalid=0;
wire s_tready;
client_tx_s #(.JUMBO_DW(4)) dut (
    .clk        (clk),
    .rx_ready   (rx_ready),
    .tx_ack     (tx_ack),
    .tx_warn    (tx_warn),
    .tx_req     (tx_req),
    .tx_len     (tx_len),
    .packet_out (packet_out),
    .s_tdata    (s_tdata),
    .s_tvalid   (s_tvalid),
    .s_tready   (s_tready)
);

integer s_len=24;
reg [4:0] s_cnt=0;
wire strobe = (s_cnt < s_len);
always @(posedge clk) begin
    if (s_tready) begin
        s_tdata <= strobe ? $random : 8'hxx;
        s_tvalid <= strobe;
        s_cnt <= s_cnt + 1;
    end
end

reg tx_strobe = 0;
reg [8*32-1:0] reply=0;
integer ccc=0;
reg fail=0;
reg [31:0] read_verify=0;
reg [23:0] read_addr=0;
integer rc_bytes=0;
always @(posedge clk) begin
    ccc <= cc%150;
    rx_ready <= ccc==10;
	tx_warn <= (ccc>64) & (ccc<(64+s_len+1));
	tx_strobe <= tx_warn;
    if (tx_strobe) begin
        rc_bytes <= rc_bytes + 1;
        reply <= {reply[8*31-1:0], packet_out};
        $display("Time: %8g ns, packet_out: 0x%8x", $time, packet_out);
    end
    if (dut.full) begin
        $display("Time: %8g ns, buffr full.", $time);
        $display("received bytes: %8d.", rc_bytes);
    end
end

endmodule
